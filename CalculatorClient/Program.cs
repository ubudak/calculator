﻿using System;
using Calculator;
using Grpc.Core;

namespace CalculatorClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var channel = new Channel("127.0.0.1:30051", ChannelCredentials.Insecure);

            var client = new Calculator.Calculator.CalculatorClient(channel);

            var reply = client.Add(new AddRequest { Value1 = 3, Value2 = 5 });

            Console.WriteLine("Sum: " + reply.Result);

            channel.ShutdownAsync().Wait();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
