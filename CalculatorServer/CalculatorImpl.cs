﻿using Calculator;
using Grpc.Core;
using System.Threading.Tasks;

namespace CalculatorServer
{
    public class CalculatorImpl:Calculator.Calculator.CalculatorBase
    {
        public override Task<AddReply> Add(AddRequest request, ServerCallContext context)
        {
            return Task.FromResult(new AddReply { Result = request.Value1 + request.Value2 });
        }
    }
}
