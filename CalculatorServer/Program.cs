﻿using Grpc.Core;
using System;

namespace CalculatorServer
{
    public class Program
    {
        private const int Port = 30051;

        public static void Main(string[] args)
        {
            var server = new Server
            {
                Services = { Calculator.Calculator.BindService(new CalculatorImpl()) },
                Ports = { new ServerPort("localhost", Port, ServerCredentials.Insecure) }
            };

            server.Start();

            Console.WriteLine("Calculator server listening on port " + Port);

            Console.WriteLine("Press any key to stop the server...");
            Console.ReadKey();

            server.ShutdownAsync().Wait();
        }
    }
}
